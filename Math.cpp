
#include "Math.h"

class Player {
private:
	string name;
	int points;

public:
	void setName(string str) {
		name = str;
	}
	void setPoints(int p) {
		points = p;
	}

	string getName() {
		return name;
	}

	int getPoints() {
		return points;
	}
	Player() : name("Player0"), points(0)
	{}

};

long long sum(long long a, long long b)
{
    return a + b;
}

double square(double a)
{
    return a * a;
}

void strcheck(std::string str) {
    
    std::cout << "The length of string is = " << str.length() << "\n";
    std::cout << "The the first character of the string is = " << str.front() << "\n";
    std::cout << "The the last  character of the string is = " << str.back() << "\n";

}
void findEvenOrOdd(int N, bool b) { // N - end of the raw; b==0 - to find all even, b==1 to find all odd
    for (int i = b; i <= N; i = i + 2) {
        cout << i << endl;
    }
}

void playerList() {

	int n = 1;
	string temp_str = "";
	int temp_int = 0;

	cout << "Enter number of players (digits only) = ";
	cin >> n;


	Player* vP = new Player[n + 1];

	for (int i = 0; i < n; i++) {

		cout << " Enter the name of Player " << i + 1 << " : ";
		cin >> temp_str;
		vP[i].setName(temp_str);

		cout << " Enter number of " << vP[i].getName() << "'s points : ";
		cin >> temp_int;
		vP[i].setPoints(temp_int);

		for (int j = 0; j <= i; j++) { // сортировка пузырьком
			if (vP[i].getPoints() > vP[j].getPoints()) {
				vP[n] = vP[j];
				vP[j] = vP[i];
				vP[i] = vP[n];
			}
		}
	}

	for (int i = 0; i < n; i++) {

		cout << vP[i].getName() << " " << vP[i].getPoints() << endl;

	}

	delete[] vP;
}
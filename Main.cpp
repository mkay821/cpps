
#include "Math.h"

class Animal {
public:
	virtual void Voice() {
		cout << "Base Class Animal\n";
	}

};

class Cat : public Animal {
public:
	void Voice() override {
		cout << "Myaaaa\n";
	}
	
};


class Dog : public Animal {
public:
	void Voice() override {
		cout << "Woof\n";
	}
};


class Cow : public Animal {
public:
	void Voice() override {
		cout << "Moooooo\n";
	}
};

int main() {

	Animal* b = new Cat;
	Animal* c = new Dog;
	Animal* d = new Cow;
	Animal* a[] = {b ,c, d};
	
	for (int i =0 ; i < 3; i++) a[i]->Voice();
	
	
	
	/*Animal* c = new Dog;
	Animal* a = new Animal[3];
	a[0] = c;
	a[0].Voice();*/

	return 0;
}